package com.leiyihui.trojanow_yl;

import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import java.io.IOException;
import java.io.InputStream;

/**
 Service layer component, but this class is a little different. It will connect to serverConnectivity component
 to set up a content view of your posts and others from the data retrieved from server.
 */
public class ContentsActivity extends ActionBarActivity {

    /**
     * initialize a list view of posts according to string index.
     * The corresponding UI component is activity_contents.xml.
     * In this function, it will connect to server to retrieve
     * all the posts available and call an adapter to set up view
     * of each post.
     */
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contents);

        String url = "dummyURL";
        InputStream is = new InputStream() {
            @Override
            public int read() throws IOException {
                return 0;
            }
        };
        ServerConnectivity conn = new ServerConnectivity(url);
        if(conn.connectServer()) {
            is = conn.download();
        }
        String[] items = inputParser(is);
        setupListAdapter(items);
    }

    /**
     * to parse input JSON data to string array to be indexed.
     */
    public String[] inputParser(InputStream is) {
        return null;
    }

    /**
     * to call MyListAdapter to set up each post to a custom view
     * according to the string index.
     */
    public void setupListAdapter(String[] items) {

    }

    /**
     * To initialize a flow menu.Can add any items like post, photos, etc.
     * to the drop-down menu.
     */
    public boolean onCreateOptionsMenu(Menu menu) {
        return super.onCreateOptionsMenu(menu);
    }

    /**
     * specific each item's action after selected. For example, if there's an
     * item called post, when post is selected, it will jump to PostActivity.
     */
    public boolean onOptionsItemSelected(MenuItem item) {
        return super.onOptionsItemSelected(item);
    }
}
