package com.leiyihui.trojanow_yl;

import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;

/**
 * Sensor Center component integrated APIs to all the supported device sensors. Business layer
 * connect to this component to perform post service. SensorType variable determines which
 * sensor data you want to retrieve.
 */
public class SensorCenter implements SensorEventListener{

    private String sensorType;
    private String temperature;
    private String location;

    /**
     * dummy constructor
     */
    public SensorCenter() {

    }

    /**
     * listening to the sensor change event to retrieve
     * different sensor data.
     */
    public void onSensorChanged(SensorEvent event) {

    }

    /**
     * update sensor data with different accuracy
     */
    public void onAccuracyChanged(Sensor sensor, int accuracy) {

    }

    /**
     * set specific sensor type as user demand
     */
    public void setSensorType(String sensorType) {
        this.sensorType = sensorType;
    }

    /**
     * get temperature
     */
    public String getTemperature() {
        return temperature;
    }

    /**
     * get location
     */
    public String getLocation() {
        return location;
    }
}
