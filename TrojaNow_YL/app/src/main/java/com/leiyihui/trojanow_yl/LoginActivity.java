package com.leiyihui.trojanow_yl;

import android.content.Intent;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;

import java.util.ArrayList;

/**
    Service layer component which is responsible for listening to the UI component input, parsing UI
    input data to a system accepted format, passing data to the business layer to be served according
    to the specific service needed.
 */
public class LoginActivity extends ActionBarActivity {

    EditText username_in;    //  This is where input from UI component passed to
    EditText password_in;    //  This is where input from UI component passed to

    /**
        initialize the login screen view according to the layout
        component activity_login.xml, and call the presentation
        logic function to get a bridge from inputs of a UI component
        to system accepted data format
     */
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        getInput();
    }

    /**
        presentation logic function which bridge the UI component inputs
        to system data representation
     */
    private void getInput() {
        username_in = (EditText) findViewById(R.id.username);
        password_in = (EditText) findViewById(R.id.password);
    }

    /**
     * This function is listening to the UI component login button click action. When clicked,
     * it will connect to the business layer component to perform login function. If
     * success, the UI will jump to content activity.
     */
    public void onClick_login(View view) {
        ArrayList<String> data = new ArrayList<>();
        data.add(username_in.getText().toString());
        data.add(password_in.getText().toString());
        Business business = new Business("login", data);
        if(business.getServed(this)) {
            Intent intent = new Intent(this, ContentsActivity.class);
            // Can add putExtra method here to parse user info to the ContentActivity.class
            startActivity(intent);
        }
        else {
            // Wrong user input info, display help message
        }
    }

    /**
     * This function is listening to the UI component signup button click action. When clicked,
     * it will jump to signup activity.
     */
    public void onClick_signup(View view) {
        Intent intent = new Intent(this, SignupActivity.class);
        startActivity(intent);
    }
}
