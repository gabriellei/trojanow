package com.leiyihui.trojanow_yl;

import java.io.InputStream;
import java.io.OutputStream;

/**
 * Server connectivity component is responsible for set up connection between client and server. It
 * manages the upload and download action of client side.
 */
public class ServerConnectivity {

    private String url;

    /**
     * constructor to initialize the server url.
     */
    public ServerConnectivity(String url) {
        this.url = url;
    }

    /**
     * request to connect to the server. return true
     * if succeed; return false otherwise;
     */
    public boolean connectServer() {
        return true;
    }

    /**
     * upload output stream to the server or it
     * can be a request
     */
    public boolean upload(OutputStream os) {
        return true;
    }

    /**
     * download input stream from the server or it
     * can be a respond
     */
    public InputStream download() {
        return null;
    }
}
