package com.leiyihui.trojanow_yl;

/**
 * This class is a data node for user info. Easy for changing user
 * info content that needed to be stored.
 */
public class UserNode {

    private String username;
    private String password;

    /**
     * Dummy constructor
     */
    public UserNode() {
    }

    /**
     * Construct UserNode object according to the info
     * passed in.
     */
    public UserNode(String username, String password) {
        this.username = username;
        this.password = password;
    }

    /**
     * set username
     */
    public void setUsername(String username) {
        this.username = username;
    }

    /**
     * set password
     */
    public void setPassword(String password) {
        this.password = password;
    }

    /**
     * get username
     */
    public String getUsername() {
        return username;
    }

    /**
     * get password
     */
    public String getPassword() {
        return password;
    }
}
