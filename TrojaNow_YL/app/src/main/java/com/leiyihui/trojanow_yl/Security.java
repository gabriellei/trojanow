package com.leiyihui.trojanow_yl;

/**
 * Security component which is responsible for authentication and validation.
 * May add other security functions in the future.
 */
public class Security {

    private String username;
    private String password;
    private DataAccess db;

    /**
     * initialize the username, password and database needed to perform
     * security check.
     */
    public Security(String username, String password, DataAccess db) {
        this.username = username;
        this.password = password;
        this.db = db;
    }

    /**
     * check if the username from user input meets certain criteria.
     * If valid, return true; otherwise, return false.
     */
    public boolean isValidUsername() {
        return true;
    }

    /**
     * check if the password from user input meets certain criteria.
     * If valid, return true; otherwise, return false.
     */
    public boolean isValidPassword() {
        return true;
    }

    /**
     * When signing up, it should perform duplicate check to see if a
     * username is available.It will connect to data access layer component
     * to see if a user input username is already existed in the database.
     */
    public boolean checkUsernameDup() {
        if(db.getEntry(username))
            return true;
        else
            return false;
    }

    /**
     * Before login, it should check if a user already signed up.
     */
    public boolean checkUsername() {
        if(db.getEntry(username))
            return true;
        else
            return false;
    }

    /**
     * Before login, it should check if the password is correct to
     * a certain username.
     */
    public boolean checkPassword() {
        return password.equals(db.getPassword(username));
    }

}
