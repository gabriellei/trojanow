package com.leiyihui.trojanow_yl;

import android.content.Context;

import java.util.ArrayList;

/**
 * Business layer component which integrates multiple business service functions.
 * The responsibility of this component is to abstract the logic required to access
 * the underlying data stores according to different service types..
 */
public class Business {

    private static final String DATABASE_NAME_LOGIN = "login.db"; // local cache(local sqlite database) to store local login info
    private static final String DATABASE_NAME_POST = "post.db";   // local cache(local sqlite database) to store local post contents

    private String type; //required service type
    private ArrayList<String> data; // uniformed data passing

    /**
     * Constructor to initialize specific type of service and data required
     */
    Business(String type, ArrayList<String> data) {
        this.type = type;
        this.data = new ArrayList<>(data);
    }

    /**
     * This function is to choose the specific service needed according to
     * specific service type.
     */
    public boolean getServed(Context context) {
        switch (type) {
            case "login":
                return loginService(this.data, context);
            case "signup":
                return signupService(this.data, context);
            case "post":
                return postService(this.data, context);
            default: return false;
        }
    }

    /**
     * This function performs login service. It will connect to
     * the DataAccess layer to retrieve data. Also it will connect
     * to Security component to perform authentication.
     */
    public boolean loginService(ArrayList<String> data, Context context) {
        DataAccess db = new DataAccess(context,DATABASE_NAME_LOGIN, null, 1);
        Security check = new Security(data.get(0), data.get(1), db);
        if(check.checkUsername() && check.checkPassword())
            return true;
        else
            return false;
    }

    /**
     * This function performs sign up service. It will connect to
     * the DataAccess layer to store data. Also it will connect
     * to Security component to perform validation.
     */
    public boolean signupService(ArrayList<String> data, Context context) {
        DataAccess db = new DataAccess(context,DATABASE_NAME_LOGIN, null, 1);
        Security check = new Security(data.get(0), data.get(1), db);
        if(check.isValidUsername() && check.checkUsernameDup() && check.isValidPassword()) {
            UserNode user = new UserNode(data.get(0), data.get(1));
            db.addUser(user);
            BackgroundService service = new BackgroundService("login");
            service.syncDB();
            return true;
        }
        else
            return false;
    }

    /**
     * This function performs post service. It will connect to
     * the DataAccess layer to store data. It also calls a parser
     * function to parse data to JSON for uploading to the server.
     */
    public boolean postService(ArrayList<String> data, Context context) {
        DataAccess db = new DataAccess(context,DATABASE_NAME_POST, null, 1);
        String jsonStr = arrayList2JSON(data);
        db.addPost(jsonStr);
        BackgroundService service = new BackgroundService("post");
        service.syncDB();
        return true;
    }

    /**
     * Parser function to parse post data to JSON string.
     */
    public String arrayList2JSON(ArrayList<String> data) {
        return null;
    }

}
