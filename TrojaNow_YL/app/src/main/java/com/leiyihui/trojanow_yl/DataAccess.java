package com.leiyihui.trojanow_yl;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * DataAccess layer component. It is responsible for provide direct entries
 * to the database including retrieving data from local database and store data
 * into local database.
 */
public class DataAccess extends SQLiteOpenHelper {

    /**
     * Constructor. Initialize DataAccess layer component.
     */
    public DataAccess(Context context, String name, SQLiteDatabase.CursorFactory factory, int version) {
        super(context, name, factory, version);
    }

    /**
     * In this function, local database tables will be created
     * include table for store login info and table for store
     * posts info.
     */
    public void onCreate(SQLiteDatabase db) {

    }

    /**
     * This function is to upgrade the existed database to a new
     * version.
     */
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }

    /**
     * This function is to store user info to the login database.
     */
    public void addUser(UserNode user){

    }

    /**
     * This function is to store posts info, more specifically,
     * JSON string to the post database.
     */
    public void addPost(String s) {

    }

    /**
     * This function is to query to see if certain username
     * tuple exists in the login database. It will return
     * true if exists, otherwise return false. Only dummy
     * return value now.
     */
    public boolean getEntry(String username){
           return true;
    }

    /**
     * This function is to query login database.
     * Select the password attribute where corresponding to
     * certain username. Return the password, if not exist,
     * return null. Only dummy return value now.
     */
    public String getPassword(String username) {
        return null;
    }
}
