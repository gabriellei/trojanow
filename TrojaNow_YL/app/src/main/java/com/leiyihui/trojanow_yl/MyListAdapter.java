package com.leiyihui.trojanow_yl;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;

/**
 * Adapter class to specify each row content(post) representation.
 */
class MyListAdapter extends ArrayAdapter<String>{

    /**
     * specify each post item according to the layout custom_row.xml.
     */
    MyListAdapter(Context context, String[] items) {
        super(context, R.layout.custom_row, items);
    }

    /**
     * to set up each row content presented. Like filling the profile image,
     * username area, post status text area, environment info area, etc.
     */
    public View getView(int position, View convertView, ViewGroup parent) {
        return super.getView(position, convertView, parent);
    }
}
