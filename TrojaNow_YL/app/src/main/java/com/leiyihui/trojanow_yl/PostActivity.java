package com.leiyihui.trojanow_yl;

import android.content.Intent;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;

import java.util.ArrayList;

/**
 Service layer component which is responsible for listening to the UI component input, parsing UI
 input data to a system accepted format, passing data to the business layer to be served according
 to the specific service needed.
 */
public class PostActivity extends ActionBarActivity {

    EditText text_post; //  This is where input from UI component passed to
    SensorCenter sensor; //  This is where input from UI component passed to
    ArrayList<String> data; // Stores the entire data of one post

    /**
     initialize the login screen view according to the layout
     component activity_post.xml, and call the presentation
     logic function to get a bridge from inputs of a UI component
     to system accepted data format
     */
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_post);
        getInput();
    }

    /**
     presentation logic function which bridge the UI component inputs
     to system data representation
     */
    private void getInput() {
        text_post = (EditText) findViewById(R.id.text_post);
        sensor = new SensorCenter(); // initialize the sensor center
        data = new ArrayList<>();
    }

    /**
     * listening to the anonymous checkbox, if checked, retrieve location
     * data from sensor center and stores it in post data.
     */
    public void onChecked_anon(View view) {
        sensor.setSensorType("location");
        String loc = sensor.getLocation();
        data.add(loc);
    }

    /**
     * listening to the environment checkbox, if checked, retrieve temperature
     * data from sensor center and stores it in post data.
     */
    public void onChecked_env(View view) {
        sensor.setSensorType("temperature");
        String temp = sensor.getTemperature();
        data.add(temp);
    }

    /**
     * This function is listening to the UI component post button click action. When clicked,
     * it will connect to the business layer component to perform post function. If
     * success, the UI will jump to content activity.
     */
    public void onClick_post(View view) {
        data.add(text_post.getText().toString());
        Business business = new Business("post", data);
        if(business.getServed(this)) {
            Intent intent = new Intent(this, ContentsActivity.class);
            startActivity(intent);
        }
        else {
            // Wrong post, display help message
        }
    }
}