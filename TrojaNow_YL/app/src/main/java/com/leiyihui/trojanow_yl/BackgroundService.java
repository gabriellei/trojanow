package com.leiyihui.trojanow_yl;

/**
 * Background Service component is a standalone component that perform
 * asynchronous tasks like sync local database with remote server. Also
 * it can be extended to perform notification service.
 */
public class BackgroundService{

    private String serviceType;

    /**
     * initialize background service. For now, service type limited to
     * login and post which perform sync of local login database
     * and sync of local post database with remote server.
     */
    public BackgroundService(String serviceType) {
        this.serviceType = serviceType;
    }

    /**
     * sync with remote server. This function will connect to server connectivity component
     * to allow it to perform client/server tasks like request and respond, upload and download.
     */
    public void syncDB() {

    }
}
